Given(/^I am on Trueaf(\d+) page$/) do |arg1|
  visit('/')
end
Given(/^I am on Login$/) do
  visit('http://trueid.truelife.com/userv4/accounts/th?ref=http://trueaf.truelife.com/af10/clip-concert/2888343/true-af10-concert-week3')
end

When(/^I click Login Button$/) do
  page.should have_xpath("//a[@id='share_login']")
  find(:xpath, "//a[@id='share_login']").click
end

Then(/^I am on Login page$/) do
  page.should have_xpath("//input[@id='username']")
  page.should have_xpath("//input[@id='trueid_password']")
end

When(/^I fill Username "(.*?)"$/) do |arg1|
  fill_in('username', :with => arg1)
end

When(/^I fill Password "(.*?)"$/) do |arg1|
  fill_in('trueid_password', :with => arg1)
end

When(/^I click Button "(.*?)"$/) do |arg1|
  page.should have_xpath("//a[@class='btn_login float-right']")
  find(:xpath, "//a[@class='btn_login float-right']").click
end

Then(/^I should on Trueaf(\d+) page$/) do |arg1|
  page.should have_xpath("//div[@id='profile_name']")
end

Then(/^I see DisplayName "(.*?)"$/) do |arg1|
  page.should have_xpath("//div[@id='profile_name']", :text => arg1)
end